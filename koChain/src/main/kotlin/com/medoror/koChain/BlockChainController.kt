package com.medoror.koChain

import com.medoror.koChain.ResponseService.Companion.createResponse
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

import org.springframework.web.client.RestTemplate
import java.net.MalformedURLException
import java.net.URL
import java.util.*

@CrossOrigin
@RestController
class BlockChainController(var blockChain: BlockChain, var nodeIdentifier: UUID = UUID.randomUUID()) {


  @PostMapping("/transactions/new")
  fun newTransaction(@RequestBody transaction: TransactionDetail): ResponseEntity<String> {
    val index =
      blockChain.newTransaction(transaction.sender, transaction.recipient, transaction.amount)
    return createResponse("The transaction will be added to block $index", HttpStatus.CREATED)
  }

  @GetMapping("/mine")
  fun mine(): ResponseEntity<Block> {
    val lastBlock = blockChain.lastBlock()
    val lastProof = lastBlock.proof
    val proof = blockChain.proofOfWork(lastProof)

    blockChain.newTransaction("0", nodeIdentifier.toString(), 1)

    val previousHash = BlockChain.hash(lastBlock)

    return createResponse(blockChain.newBlock(proof, previousHash), HttpStatus.CREATED)
  }

  @PostMapping("/nodes/register")
  fun registerNodes(@RequestBody urls: Nodes): ResponseEntity<String> {
    try {
      for (url in urls.urls) {
        blockChain.registerNodes(URL(url))
      }
    } catch (e: MalformedURLException) {
      return createResponse("Error: please supply a valid list of nodes", HttpStatus.BAD_REQUEST)
    }
    return createResponse("New node list after update " + blockChain.nodeSet.joinToString(", "),
      HttpStatus.CREATED)
  }

  @GetMapping("/chain")
  fun fullChain(): ResponseEntity<BlockChainDetail> {
    return createResponse(blockChain.fullChain(), HttpStatus.OK)
  }

  @GetMapping("/nodes/resolve")
  fun resolve(): ResponseEntity<String> {
    if (blockChain.resolveConflicts()) {
      return createResponse("Our chain was replaced \n" + blockChain.chain.joinToString(", "),
        HttpStatus.OK)
    } else {
      return createResponse("Our chain was authoritative \n" + blockChain.chain.joinToString(", "),
        HttpStatus.OK)
    }
  }

  @GetMapping("/reset")
  fun resetChain(): ResponseEntity<String> {
    blockChain = BlockChain()
    nodeIdentifier = UUID.randomUUID()!!

    return createResponse("Blockchain has been reset", HttpStatus.OK)
  }
}
