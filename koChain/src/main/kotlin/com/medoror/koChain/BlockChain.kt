package com.medoror.koChain

import com.fasterxml.jackson.module.kotlin.*
import org.apache.commons.codec.digest.DigestUtils
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.client.RestTemplate
import java.net.URL
import java.util.*
import kotlin.collections.ArrayList

@Component
data class BlockChain(var currentTransactions: MutableList<TransactionDetail> = mutableListOf<TransactionDetail>(),
                      var chain: MutableList<Block> = mutableListOf<Block>(),
                      var nodeSet: MutableSet<URL> = mutableSetOf<URL>()) {

  init {
    newBlock(100, "1")
  }

  companion object {
    /**
     * Takes a block object and uses its attributes to create a hash string
     */
    @JvmStatic
    fun hash(block: Block): String {

      val mapper = jacksonObjectMapper()
      val writer = mapper.writerWithDefaultPrettyPrinter()

      val json = writer.writeValueAsString(block).toByteArray()

      return DigestUtils.sha256Hex(json)
    }

    /**
     * Proof validation method.  Will return true if the hash 4 trailing zeroes
     */
    @JvmStatic
    fun validProof(last_proof: Long, proof: Long): Boolean {

      val guess = encoder(last_proof.toString() + proof.toString())

      val guessHash = DigestUtils.sha256Hex(guess)

      return guessHash.takeLast(4) == "0000"
    }

    /**
     * Private method that takes a string and returns its base 64 encoding
     */
    @JvmStatic
    private fun encoder(toEncode: String): String {
      val bytes = toEncode.toByteArray()
      return Base64.getEncoder().encodeToString(bytes)
    }
  }

  /**
   * Create a new block
   * @param proof
   * @param previous_hash
   * @return Block - new block
   */
  fun newBlock(proof: Long, previous_hash: String?): Block {

    // create new block
    val block = Block(1 + chain.size,
      Date().time,
      ArrayList(currentTransactions),
      proof,
      previous_hash ?: BlockChain.hash(chain.last()))

    // reset the current list of transactions
    currentTransactions.clear()

    chain.add(block)

    return block
  }

  /**
   * Returns the full chain of blocks
   * @return BlockChainDetail
   */
  fun fullChain(): BlockChainDetail {
    return BlockChainDetail(chain, chain.size);
  }

  /**
   * Creates a new transaction that will be added to the next block
   * @param sender
   * @param recipient
   * @param amount
   * @return Integer - next index
   */
  fun newTransaction(sender: String, recipient: String, amount: Int): Int {
    currentTransactions.add(TransactionDetail(sender, recipient, amount))

    return lastBlock().index + 1
  }

  /**
   * Convenience method to return the last block
   * @return Block
   */
  fun lastBlock(): Block {
    return chain.last()
  }

  /**
   * Find a proof that when hashed returns a hash that contains trailing 4 zeroes
   * @param last_proof
   */
  fun proofOfWork(last_proof: Long): Long {
    var proof = 0L
    while (!validProof(last_proof, proof)) {
      proof += 1
    }
    return proof
  }

  /**
   * Adds a url to the set urls for this node
   * @param address
   */
  fun registerNodes(address: URL) {
    nodeSet.add(address)
  }

  /**
   * Takes a chain and validates its hashes from the previous block hash
   * @param chain
   * @return Boolean - true if the chain is valid
   */
  fun validChain(chain: MutableList<Block>): Boolean {
    var currentIndex = 1
    var lastBlock = chain[0]

    while (currentIndex < chain.size) {
      var block = chain[currentIndex]

      if (block.previousHash != hash(lastBlock)) return false

      if (!validProof(lastBlock.proof, block.proof)) return false

      currentIndex++
      lastBlock = block
    }
    return true
  }

  /**
   * Consensus Algorithm - Will attempt to resolve conflicts by replacing all chains with
   * the longest in the network
   * @return Boolean - true if the chain should be replaced
   */
  fun resolveConflicts(): Boolean {

    val restTemplate = RestTemplate()

    var neighbors = nodeSet
    var newChain = mutableListOf<Block>()

    var maxLength = chain.size
    for (node in neighbors) {
      var response = restTemplate.exchange(node.toString() + "/chain",
        HttpMethod.GET, null, BlockChainDetail::class.java)

      if (response.statusCode == HttpStatus.OK) {
        var chainDetail = response.body

        //TODO: why is the response body nullable?

        var length = chainDetail?.length
        var chain = chainDetail?.chain

        if (length!! > maxLength && validChain(chain!!)) {
          maxLength = length
          newChain = chain
        }
      }
      if (!newChain.isEmpty()) {
        this.chain = newChain
        return true
      }
    }
    return false
  }

}
