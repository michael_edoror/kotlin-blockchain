package com.medoror.koChain

import org.apache.logging.log4j.util.Strings
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity

class ResponseService {
  companion object {
    @JvmStatic
    fun <T : Any?> createResponse(body: T, statusCode: HttpStatus): ResponseEntity<T> {
      return ResponseEntity(body, statusCode)
    }

    @JvmStatic
    fun <T : Any?> createResponseWrapper(entity: ResponseEntity<T>): ResponseWrapper<ResponseEntity<T>> {
      return ResponseWrapper<ResponseEntity<T>>(entity, "")
    }

    @JvmStatic
    fun <T : Any?> createResponseWrapper(message: String, entity: ResponseEntity<T>): ResponseWrapper<ResponseEntity<T>> {
      return ResponseWrapper<ResponseEntity<T>>(entity, message)
    }
  }

  data class ResponseWrapper<T>(var response: T, var message: String = Strings.EMPTY)

}
