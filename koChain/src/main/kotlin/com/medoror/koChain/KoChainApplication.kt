package com.medoror.koChain

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class KoChainApplication

fun main(args: Array<String>) {
  runApplication<KoChainApplication>(*args)
}
