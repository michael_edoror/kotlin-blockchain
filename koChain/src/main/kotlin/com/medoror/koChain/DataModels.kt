package com.medoror.koChain

data class Block(var index: Int, var timeStamp: Long,
                 var transactions: MutableList<TransactionDetail> = mutableListOf<TransactionDetail>(),
                 var proof: Long, var previousHash: String)

data class TransactionDetail(var sender: String, var recipient: String,
                             var amount: Int)

data class Nodes(var urls: MutableList<String> = mutableListOf<String>())

data class BlockChainDetail(var chain: MutableList<Block> = mutableListOf<Block>(),
                            var length: Int)
