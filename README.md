# Building a blockchain with Kotlin

This is source code of a blockhain implementation based from this [page](http://michaeledoror.com/2018/05/05/learning-blockchain-using-kotlin/) and this [tutorial](https://medium.com/p/117428612f46)

## Installation
1. Clone this repository
2. cd kochain/
3. mvn clean install 
4. Run the server on port 8080: `$java -jar target/koChain-1.0.0.jar`
	 
5. Run the server on a different port by copying the jar to a different directory for example:
	 `$java -jar -Dserver.port=9999 target/koChain-1.0.0.jar`

## Testing
Enpoints can be tested using [postman](https://www.getpostman.com/).  After cloning this repository, import the postman file from  `$ koChain/src/main/resources/koChain.postman_collection.json` into your local postman installation

## TODO
Fix docker configuration. How do docker containers talk to each other?
